import React, { Component } from 'react'

export default class TimelineDateComponent extends Component {
    render() {
        return (
            <li>
                <div className="tldate">{this.props.year}</div>
            </li>

        )
    }
}
