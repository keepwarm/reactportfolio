import React, { Component } from 'react'
import './TimelineComponent.scss';

export default class TimelineComponent extends Component {
    render() {
        return (
            <ul className="timeline">
                {this.props.children}
            </ul>
        )
    }
}
