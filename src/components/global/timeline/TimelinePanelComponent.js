import React, { Component } from 'react'

export default class TimelinePanelComponent extends Component {
    render() {
        return (
            <li className={this.props.inverted ? 'timeline-inverted' : ''}>
                <div className="tl-circ"></div>
                <div className="timeline-panel">
                    <div className="tl-heading">
                        <h4>{this.props.title}</h4>
                        <p><small className="text-muted"><i className="far fa-calendar-alt"></i>{this.props.year}</small></p>
                    </div>
                    <div className="tl-body">
                        {this.props.children}
                    </div>
                </div>
            </li>
        )
    }
}
