import React, { Component } from 'react';
import './CircleTextComponent.css';

export default class CircleTextComponent extends Component {
    render() {
        return (
            <div class="circle-text">{this.props.children}</div>
        )
    }
}
