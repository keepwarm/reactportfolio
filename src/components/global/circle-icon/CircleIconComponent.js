import React, { Component } from 'react';
import './CircleIconComponent.css';

export default class CircleIconComponent extends Component {
    getSizeCss() {
        switch(this.props.size){
            case 'large':
                return 'fa-stack fa-stack-large fa-2x';
            case 'medium':
                return 'fa-stack fa-stack-medium fa-2x';
            default: 
                return 'fa-stack fa-stack-small fa-2x circle'
        }
    }

    getIconCss(){
        let iconSet = 'fab';
        if (this.props.iconSet){
            iconSet = this.props.iconSet;
        }
        return `${iconSet} fa-stack-1x ${this.props.icon}`;
    }

    render() {
        return (
            <span className = {this.getSizeCss()}>
                <i className='fas fa-circle fa-stack-2x' style={{ color: this.props.color}}></i>
                <i className= {this.getIconCss()}></i>
            </span>
        )
    }
}
