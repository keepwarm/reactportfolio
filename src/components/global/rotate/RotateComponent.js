import React, { Component } from 'react';
import './RotateComponent.scss';

export default class RotateComponent extends Component {
    render() {
        return (
            <div>
                <span className='rotatingText-adjective'>{this.props.content1}</span>
                <span className='rotatingText-adjective'>{this.props.content2}</span>
                <span className='rotatingText-adjective'>{this.props.content3}</span>
            </div>
        )
    }
}
