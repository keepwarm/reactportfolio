import React, { Component } from 'react'
import CircleIconComponent from '../../global/circle-icon/CircleIconComponent'
import CircleTextComponent from '../../global/circle-text/CircleTextComponent'

export default class SkillsSectionComponent extends Component {
    render() {
        return (
            <section id="skills" className="py-5" style={{backgroundColor: '#9fa9a3'}}>
                <div className="container">
                    <div className="row mb-5">
                        <div className="col">
                            <h2 className="text-center front-weight-bolder" style={{fontSize: '3em'}}>SKILLS</h2>
                        </div>
                    </div>
                    <div className="row mb-5"> 
                        <div class = "col d-flex justify-content-around"> 
                            <CircleIconComponent 
                                color = "lightgrey"
                                size = "large"
                                icon = "fa-js-square"
                            />
                            <CircleIconComponent 
                                color = "lightgrey"
                                size = "large"
                                icon = "fa-html5"
                            />
                            <CircleIconComponent 
                                color = "lightgrey"
                                size = "large"
                                icon = "fa-css3-alt"
                            /> 
                        </div>
                    </div>
                    <div className="row mb-5">
                        <div className="col d-flex justify-content-around" style = {{marginLeft: '10rem', marginRight: '10rem'}}>
                            <CircleIconComponent
                                color="lightgrey"
                                size="medium"
                                icon="fa-react"
                            />

                            <CircleIconComponent
                                color="lightgrey"
                                size="medium"
                                icon="fa-node-js"
                            />

                            <CircleIconComponent
                                color="lightgrey"
                                size="medium"
                                icon="fa-bootstrap"
                            />
                        </div>
                    </div>
                    <div className = "row">
                        <div className ="col d-flex justify-content-around" style = {{marginLeft: '14rem', marginRight: '14rem'}}>
                            {/* <CircleIconComponent
                                color="lightgrey"
                                size="small"
                                icon="fa-react-native"
                            /> */}

                            <CircleIconComponent
                                color="lightgrey"
                                size="small"
                                icon="fa-database"
                                iconSet="fas"
                            />

                            <CircleIconComponent
                                color="lightgrey"
                                size="small"
                                icon="fa-git"
                            />
                        </div>
                    </div>
                    <div className = "row">
                        <div class = "col d-flex justify-content-around"> 
                            <CircleTextComponent>React Native</CircleTextComponent>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
