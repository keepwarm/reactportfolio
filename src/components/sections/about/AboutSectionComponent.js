import React, { Component } from 'react';
import './AboutSectionComponent.scss';
import image from '../../../assets/hien-ho.jpg';

export default class AboutSectionComponent extends Component {
    render() {
        return (
            <section id='about' style = {{paddingTop: 50, paddingBottom: 50}}>
                <div className='container'>
                    <div className='row'>
                        <div className='col'>
                            <div className='img-container'>
                                <img src= {image} alt='hien-ho' className='img-rounded img-about' style = {{width: 120}}/>
                            </div>
                            <div className='flexbox-container'>
                                <div className='flexbox-item flex-item--left'>
                                    <h2 className='my-name'> Hien Ho</h2>
                                    <span>
                                        <i className="fas fa-map-marker-alt"></i>
                                        <cite title='Source Title'>Iowa, United State</cite>
                                    </span>
                                </div>
                                <ul className='flexbox-item flex-item--right'>
                                    <li>
                                        <i className="fas fa-envelope"></i>
                                        <a href='mailto:hienho88vn@gmail.com' >hienho88vn@gmail.com</a>
                                    </li>
                                    <li>
                                        <i className="fab fa-bitbucket"></i>
                                        <a target='_blank' href="https://bitbucket.org/keepwarm/">www.bitbucket.org/hien-ho</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className='row'>
                        <div className="col">
                            <p>I am from Vietnam, I came to the US in November 2013. In my country I had a bachelor in tourism management and worked there in that field for two years
                            before I moved to the US. However, since I have been here, technology has been more interesting to me,
                            so I went back to school at Des Moines Area Community College (DMACC) and transferred to Grand View University for Management Information Systems.</p>    
                            <p>After I graduated, I secured my dream job at Principal Financial Group as a Software Engineer. 
                                I am continuing to grow my front-end development skills with HTML, CSS, JavaScript and React, while learning new ones such as Backbone.js, Dust.js and XSLT.
                                I have also begun my journey into back-end development learning Java.
                            </p>
                            <p>Thank you for spending your time to learn a little more about me.</p>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
