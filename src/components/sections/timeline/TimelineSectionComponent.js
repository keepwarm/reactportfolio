import React, { Component } from 'react'
import TimelineComponent from '../../global/timeline/TimelineComponent'
import TimelineDateComponent from '../../global/timeline/TimelineDateComponent'
import TimelinePanelComponent from '../../global/timeline/TimelinePanelComponent'
import './TimelineSectionComponent.css';
import image from '../../../assets/backgroundImageSection3.jpeg';

export default class TimelineSectionComponent extends Component {
    render() {
        return (
            <section id='timeLine' style = {{backgroundImage: `url(${image})`}}>
                <div className="container">
                    <TimelineComponent>
                        <TimelineDateComponent year="2022 - Present" />
                        <TimelinePanelComponent title="Principal Financial Group, Des Moines, USA"
                            year="September, 2022 – Present"
                            inverted
                        >
                            <h5>Software Engineer II - Fullstack</h5>
                            <p>- Develop web based applications using HTML, CSS, JavaScript, Jquery, Bootstrap, XSLT, React, JSF &amp; Java. </p>
                            <p>- Work within agile methodology</p>
                        </TimelinePanelComponent>
                        <TimelineDateComponent year="2021 - 2022" />
                        <TimelinePanelComponent title="Principal Financial Group, Des Moines, USA"
                            year="February, 2021"
                            inverted
                        >
                            <h5>Software Engineer I - Front-end Development</h5>
                            <p>- Develop web based applications using HTML, CSS, JavaScript, Jquery, Bootstrap, XSLT, Backbone, Dust.js &amp; React. </p>
                            <p>- Work within agile methodology</p>
                        </TimelinePanelComponent>

                        <TimelineDateComponent year="2019 - 2020" />
                        <TimelinePanelComponent title = "The Hong Kong University Of Science And Technology, Coursera"
                                                year = "Issued November 2020"
                                                >
                            <h5>Certificate of Full-Stack Web Development with React Specialization <small>-Credential ID AW8UJKBQEJ54</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "The Hong Kong University Of Science And Technology, Coursera"
                                                year = "Issued October 2020"
                                                >
                            <h5>Certificate of Server-side Development with NodeJS, Express and Mongo <small>-Credential ID 499CK4TCQA8P</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "The Hong Kong University Of Science And Technology, Coursera"
                                                year = "Issued September 2020"
                                                >
                            <h5>Certificate of Multiplatform Mobile App Development with React Native <small>-Credential ID YGWF5B9AL3YG</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Queen’s Web, Cypress, Texas, USA"
                                                year = "August, 2020 – December, 2020"
                                                inverted
                                                >
                            <h5>Software Engineer Internship - Remote</h5>
                            <p>- Building mobile app using React Native. </p>   
                            <p>- Working with a team that includes software engineers and UI/UX specialists. Utilized React Native, Redux, Firebase.</p> 
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "The Hong Kong University Of Science And Technology, Coursera"
                                                year = "Issued August 2020"
                                                >
                            <h5>Certificate of Front-End Web Development with React<small>- Credential ID SAR4Y94X4PVL</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "The Hong Kong University Of Science And Technology, Coursera"
                                                year = "Issued July 2020"
                                                >
                            <h5>Certificate of Front-End Web UI Frameworks and Tools: Bootstrap 4 <small>- Credential ID EN797VA2GCGK</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Des Moines Area Community College, Des Moines, IA, USA"
                                                year = "2016 - March 2020"
                                                inverted
                                                >
                            <h5>Computer Assistant - Part-time</h5>
                            <p>- Assist students with installation and use of computer software. </p>   
                            <p>- Help students use and understand the user management system.</p> 
                            <p>- Basic help desk for students.</p>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Grand View University, Des Moines, IA, USA"
                                                year = "Graduated - 04/25/2020"
                                                >
                            <h5>Bachelor Degree in Management Information Systems. <small>- Focusing on Information Technology</small></h5>
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Des Moines Area Community College, Des Moines, IA, USA"
                                                year = "Fall 2019"
                                                inverted
                                                >
                            <h5>Web Development - Internship</h5>
                            <p>- Created Academic website for surveys</p>
                            <p>- Used Accudemia to create additional surveys</p>
                        </TimelinePanelComponent>
                        
                        <TimelineDateComponent year = "2014 - 2018"/>
                        <TimelinePanelComponent title = "Des Moines Area Community College, Des Moines, IA, USA"
                                                year = "2015 - 2016"
                                                inverted
                                                >
                            <h5>Math Tutor in TRIO Program - Part-time</h5>
                            <p>- Worked with students to help gain understanding of lessons and problem solving</p  >
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Des Moines Area Community College, Des Moines, IA, USA"
                                                year = "Graduated 05/2018"
                                                >
                            <h5>Associate in Arts in Management Information Systems. <small>-Focusing on Information Technology</small></h5>
                        </TimelinePanelComponent>

                        <TimelineDateComponent year = "2010 -2013"/>
                        <TimelinePanelComponent title = "Mai Linh Tourism, Danang City, Vietnam"
                                                year = "2011 - 2013"
                                                inverted
                                                >
                            <h5>Tourism Operator - Full-time</h5>
                            <p>- Wrote the tour schedule, organized tours and prepared reports. </p>
                            <p>- Coordinated travel arrangement (meals, transportation, destination, tour-guide).</p> 
                        </TimelinePanelComponent>

                        <TimelinePanelComponent title = "Duy Tan University, Danang City, Vietnam"
                                                year = "Graduated 05/2010"
                                                >
                            <h5>- Bachelor Degree in Tourism Science</h5>
                            <h5>- Certificate of Computer Operating</h5> 
                        </TimelinePanelComponent>

                    </TimelineComponent>
                </div>
            </section>  
        )
    }
}
