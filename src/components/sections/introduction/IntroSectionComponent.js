import React, { Component } from 'react'
import NavigationComponent from '../../layout/navigation/NavigationComponent'
import './IntroSectionComponent.scss';
import image from '../../../assets/backgroundImageSection1.jpeg';
import RotateComponent from '../../global/rotate/RotateComponent';


export default class IntroSectionComponent extends Component {
    render() {
        return (
            <section id='introduction' style = {{backgroundImage: `url(${image})`}}>
                {/* <NavigationComponent /> */}

                <div className='welcome'>
                    <h1 id='introduction-header'>Welcome To My Portfolio</h1>
                </div>

                <div className='content'>
                    <RotateComponent
                        content1="My name is Hien Ho"
                        content2="My focus is on Front-end Development"
                        content3="My goals are Full-stack Development and UX Design"
                        >
                    </RotateComponent>
                </div>
            </section>
        )
    }
}

