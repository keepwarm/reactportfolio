import React from 'react';
import './NavigationComponent.scss';
import Scrollspy from 'react-scrollspy'

export default function NavigationComponent() {
    return (
        <div id='sidenav'>
            <nav className="nav nav-pills flex-column">
                <Scrollspy items={['introduction', 'about', 'skills', 'timeLine']} currentClassName="active">
                    <a className="nav-link" href='#introduction'>Welcome</a>
                    <a className="nav-link" href='#about'>About</a>
                    <a className="nav-link" href='#skills'>Skills</a>
                    <a className="nav-link" href='#timeLine'>Timeline</a>
                </Scrollspy>
            </nav>
        </div>
    )
}
