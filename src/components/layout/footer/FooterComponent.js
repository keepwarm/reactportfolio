import React from 'react';
import './FooterComponent.scss';

export default function FooterComponent() {
    const year = new Date().getFullYear();
    return (
        <section id='footer'>
            <div className='quote'>
                "The only person you should try to be better than is the person you were yesterday."
            </div>

            <div class='ocean'>
                <div className='wave'></div>
                <div id='copyright'>
                    Copyright <i className="far fa-copyright"></i> {year} by Hien Ho
                </div>
            </div>
        </section>
    )
}
