import React from 'react';
// import logo from './logo.svg';
import './App.css';
import NavigationComponent from './components/layout/navigation/NavigationComponent';
import IntroSectionComponent from './components/sections/introduction/IntroSectionComponent';
import AboutSectionComponent from './components/sections/about/AboutSectionComponent';
import TimelineSectionComponent from './components/sections/timeline/TimelineSectionComponent';
import FooterComponent from './components/layout/footer/FooterComponent';
import SkillsSectionComponent from './components/sections/skills/SkillsSectionComponent';


function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <>
      <NavigationComponent />
      <IntroSectionComponent />
      <AboutSectionComponent />
      <SkillsSectionComponent />
      <TimelineSectionComponent />
      <FooterComponent />
    </>
     
  );
}

export default App;
